# Basic MNIST Example
# Source: https://github.com/pytorch/examples/blob/master/mnist
# 2017/10/21 @bakasta Custom to test my image

```bash
pip install -r requirements.txt
python main.py
# CUDA_VISIBLE_DEVICES=2 python main.py  # to specify GPU id to ex. 2
```
