# Basic Machine Learning: Linearegression Example
# Study from: https://machinelearningcoban.com/2016/12/28/linearregression/

```bash
pip install -r requirements.txt
python show_data.py		# show data test
python linearregression.py	# show result