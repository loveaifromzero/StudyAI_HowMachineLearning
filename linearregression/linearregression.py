from __future__ import division, print_function, unicode_literals
import numpy as np
import matplotlib.pyplot as plt

# height(cm)
X = np.array([[147, 150, 153, 158, 163, 165, 168, 170, 173, 175, 178, 180, 183]]).T

# weight (kg)
y = np.array([[49, 50, 51,  54, 58, 59, 60, 62, 63, 64, 66, 67, 68]]).T

# Building Xbar
print('X = ', X)
one = np.ones((X.shape[0], 1))
print('one = ', one)
Xbar = np.concatenate((one, X), axis = 1)
print('Xbar = ', Xbar)

# Calculating weights of the fitting line
# w = (Xbar.T * Xbar)^-1 * (Xbar.T * y) = A^-1 * b
A = np.dot(Xbar.T, Xbar)
print('A = ', A)
b = np.dot(Xbar.T, y)
print('b = ', b)
w = np.dot(np.linalg.pinv(A), b)
print('w = ', w)

# preparing the fitting line
w0 = w[0][0]
w1 = w[1][0]
x1 = np.linspace(145, 185, 2)
y0 = w1 * x1 + w0

# test
y1 = w1 * 155 + w0
y2 = w1 * 160 + w0

print('predict of 155cm: %.2fkg, real: 52kg' %(y1))
print('predict of 155cm: %.2fkg, real: 56kg' %(y2))

# Drawing the fitting line
plt.plot(X.T, y.T, 'ro')	# data
plt.plot(x1, y0) 			# fitting line
plt.axis([140, 190, 45, 75])
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.show()
